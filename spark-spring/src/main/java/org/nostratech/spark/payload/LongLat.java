package org.nostratech.spark.payload;

import lombok.*;

/**
 * Created by rennytanuwijaya on 2/5/16.
 */
@lombok.Data
public class LongLat {

    private String id;
    private String imei;
    private String longitude;
    private String latitude;
    private String signal;
    private String time;
}
