package org.nostratech.spark.payload;

/**
 * Created by rennytanuwijaya on 2/5/16.
 */
@lombok.Data
public class Data {

    private String id;
    private String imei;
    private String att;
    private String value;
}
