package org.nostratech.spark.payload;

import lombok.Data;

/**
 * agus w on 12/14/15.
 */
@Data
public class Book {

    private String id;
    private String title;
    private String author;

}
