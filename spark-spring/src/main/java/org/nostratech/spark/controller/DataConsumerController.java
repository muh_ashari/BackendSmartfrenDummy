package org.nostratech.spark.controller;

import com.google.gson.Gson;
import org.nostratech.spark.payload.Data;
import org.nostratech.spark.payload.DataMaster;
import org.nostratech.spark.payload.LongLat;
import org.nostratech.spark.services.ConsumerDataService;

import static spark.Spark.*;

/**
 * Created by rennytanuwijaya on 2/5/16.
 */
public class DataConsumerController {

    Gson gson = new Gson();

    public DataConsumerController(final ConsumerDataService consumerDataService) {

        post("/LongLat", (request, response) -> {

            // process request
            final DataMaster deviceData = gson.fromJson(request.body(), DataMaster.class);

            RequestHandler requestHandler = new RequestHandler() {
                @Override
                public Object processRequest() {
                    return consumerDataService.post(deviceData.getLongLat());
                }
            };

            response.status(200);
            response.type("application/json");
            return requestHandler.getResult("OK");
        });


        put("/deviceData", (request, response) -> {

            // process request
            LongLat data = gson.fromJson(request.body(), LongLat.class);

            RequestHandler requestHandler = new RequestHandler() {
                @Override
                public Object processRequest() {
                    return consumerDataService.put(data);
                }
            };

            response.status(200);
            response.type("application/json");
            return requestHandler.getResult("OK");
        });

        get("/longLat", (request, response) -> {
            RequestHandler requestHandler = new RequestHandler() {
                @Override
                public Object processRequest() {
                    return consumerDataService.get();
                }
            };

            response.status(200);
            response.type("application/json");
            return requestHandler.getResult("OK");
        });

        get("/longLatByTime", (request, response) -> {
            RequestHandler requestHandler = new RequestHandler() {
                @Override
                public Object processRequest() {
                    String startTime = request.queryParams("startTime");
                    String endTime = request.queryParams("endTime");
                    return consumerDataService.getSignalTime(startTime,endTime);
                }
            };

            response.status(200);
            response.type("application/json");
            return requestHandler.getResult("OK");
        });

        get("/longLatByImei", (request, response) -> {
            RequestHandler requestHandler = new RequestHandler() {
                @Override
                public Object processRequest() {
                    String startTime = request.queryParams("startTime");
                    String endTime = request.queryParams("endTime");
                    String imei = request.queryParams("imei");
                    return consumerDataService.getSignalByImei(startTime,endTime,imei);
                }
            };

            response.status(200);
            response.type("application/json");
            return requestHandler.getResult("OK");
        });

        get("/deviceData/:id", (request, response) -> {
            RequestHandler requestHandler = new RequestHandler() {
                @Override
                public Object processRequest() {
                    return consumerDataService.get(request.params(":id"));
                }
            };

            response.status(200);
            response.type("application/json");
            return requestHandler.getResult("OK");
        });

        delete("/deviceData/:id", (request, response) -> {
            RequestHandler requestHandler = new RequestHandler() {
                @Override
                public Object processRequest() {
                    return consumerDataService.delete(request.params(":id"));
                }
            };

            response.status(200);
            response.type("application/json");
            return requestHandler.getResult("OK");
        });
    }
}
